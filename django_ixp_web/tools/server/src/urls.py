from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin

urlpatterns = patterns(
    "",
    re_path(
        r"^static/(?P<path>.*)$",
        "django.contrib.staticfiles.views.serve",
        {"insecure": True},
    ),
    re_path(
        r"^media/(?P<path>.*)$",
        "django.views.static.serve",
        {"document_root": settings.MEDIA_ROOT},
    ),
) + i18n_patterns(
    "",
    re_path(r"^admin/", include(admin.site.urls)),
    re_path(r"^", include("cms.urls")),
)
